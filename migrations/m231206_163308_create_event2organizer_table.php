<?php

use yii\db\Migration;
use app\models\Event;
use app\models\Organizer;
use app\models\Event2organizer;

/**
 * Handles the creation of table `{{%event2organizer}}`.
 */
class m231206_163308_create_event2organizer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(Event2organizer::tableName(), [
            'id' => $this->primaryKey(),
            'event_id' => $this->integer(),
            'organizer_id' => $this->integer(),
        ]);

        $this->addForeignKey(
            'event_id-fk',
            Event2organizer::tableName(),
            'event_id',
            Event::tableName(),
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'organizer_id-fk',
            Event2organizer::tableName(),
            'organizer_id',
            Organizer::tableName(),
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('event_id-fk', Event2organizer::tableName());
        $this->dropForeignKey('organizer_id-fk', Event2organizer::tableName());

        $this->dropTable(Event2organizer::tableName());
    }
}
