<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%organizer}}`.
 */
class m231206_162615_create_organizer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(\app\models\Organizer::tableName(), [
            'id' => $this->primaryKey(),
            'fio' => $this->string(250)->notNull(),
            'email' => $this->string(150)->defaultValue(null),
            'phone' => $this->integer(15)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%organizer}}');
    }
}
