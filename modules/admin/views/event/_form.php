<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\select2\Select2;

/** @var yii\web\View $this */
/** @var app\models\Event $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="event-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <label class="form-label">Event Date</label>
    <?= '';
    echo DatePicker::widget([
        'attribute' => 'event_date',
        'model' => $model,
        'type' => DatePicker::TYPE_INPUT,
        'value' => '23-Feb-1982',
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'dd-M-yyyy'
        ]
    ]); ?>

    <label class="control-label">Organizers</label>;

    <?php
    $model->organizerList = array_keys($model->getOrganizers()->indexBy('id')->column());

    echo $form->field($model, "organizerList")->widget(Select2::classname(), [
        'data' => \app\models\Organizer::find()->select('fio')->indexBy('id')->column(),
        'options' => [
            'placeholder' => 'Выберите организаторов ...',
            'multiple' => true
        ],
    ])->label(false);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
