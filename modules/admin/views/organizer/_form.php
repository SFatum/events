<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/** @var yii\web\View $this */
/** @var app\models\Organizer $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="organizer-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput() ?>

    <label class="control-label">Events</label>;

    <?php
    $model->eventList = array_keys($model->getEvents()->indexBy('id')->column());

    echo $form->field($model, "eventList")->widget(Select2::classname(), [
        'data' => \app\models\Event::find()->select('name')->indexBy('id')->column(),
        'options' => [
            'placeholder' => 'Выберите мероприятия ...',
            'multiple' => true
        ],
    ])->label(false);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
