<?php

/** @var yii\web\View $this */
/** @var \app\models\Event[] $events */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <?php if ($events): ?>
                <ul>
                <?php foreach ($events as $event): ?>
                <li>
                    <h3><?=$event->name?>, <?=date('d.m.Y', strtotime($event->event_date))?></h3>
                    <?php if ($event->organizers): ?>
                    <ul>
                        <?php foreach ($event->organizers as $organizer): ?>
                        <li><?=$organizer->fio?>, <?=$organizer->email?>, <?=$organizer->phone?></li>
                        <?php endforeach; ?>
                    </ul>
                    <?php endif; ?>
                </li>
                <?php endforeach; ?>
                </ul>
            <?php endif; ?>
        </div>

    </div>
</div>
