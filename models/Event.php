<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "event".
 *
 * @property int $id
 * @property string $name
 * @property int|null $event_date
 *
 * @property array $organizerList
 *
 * @property Organizer[] $organizers
 */
class Event extends \yii\db\ActiveRecord
{
    public $organizerList;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 250],
            [['event_date'], 'string', 'max' => 15],

            ['organizerList', 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'event_date' => 'Event Date',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $this->syncOrganizers();
    }


    /**
     * @return ActiveQuery | Organizer[]
     */
    public function getOrganizers()
    {
        return $this->hasMany(Organizer::class, ['id' => 'organizer_id'])->via('event2Organizer');
    }

    /**
     * @return ActiveQuery | Event2organizer[]
     */
    public function getEvent2Organizer()
    {
        return $this->hasMany(Event2organizer::class, ['event_id' => 'id']);
    }

    private function syncOrganizers()
    {
        Event2organizer::deleteAll(['event_id' => $this->id]);

        if ($this->organizerList) {
            foreach ($this->organizerList as $organizer) {
                $this->link('organizers', Organizer::findOne($organizer));
            }
        }
    }
}
