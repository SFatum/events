<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "organizer".
 *
 * @property int $id
 * @property string $fio
 * @property string|null $email
 * @property int $phone
 *
 * @property array $eventList
 *
 * @property Event[] $events
 */
class Organizer extends \yii\db\ActiveRecord
{
    public $eventList;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'organizer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fio', 'phone'], 'required'],
            [['phone'], 'integer'],
            [['fio'], 'string', 'max' => 250],
            [['email'], 'string', 'max' => 150],

            ['eventList', 'safe']
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $this->syncEvents();
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'Fio',
            'email' => 'Email',
            'phone' => 'Phone',
        ];
    }

    /**
     * @return ActiveQuery | Event[]
     */
    public function getEvents()
    {
        return $this->hasMany(Event::class, ['id' => 'event_id'])->via('organizer2Event');
    }

    /**
     * @return ActiveQuery | Event2organizer[]
     */
    public function getOrganizer2Event()
    {
        return $this->hasMany(Event2organizer::class, ['organizer_id' => 'id']);
    }

    private function syncEvents()
    {
        Event2organizer::deleteAll(['event_id' => $this->id]);

        if ($this->eventList) {
            foreach ($this->eventList as $event) {
                $this->link('events', Event::findOne($event));
            }
        }
    }
}
